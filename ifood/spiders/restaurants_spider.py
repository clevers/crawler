# -*- coding: utf-8 -*-

import scrapy
import json
import re

from ifood.items import Restaurant, FoodCategory, MenuItem, Address, Geo, DayOfWeek
from extract_links import rio_restaurants_links as links


class RestaurantsSpider(scrapy.Spider):
	name = "ifood"
	#start_urls = links
	start_urls = [
		links[0]
		#'https://www.ifood.com.br/delivery/rio-de-janeiro-rj/sushi-rio-botafogo'
		#'https://www.ifood.com.br/delivery/rio-de-janeiro-rj/bibi-sucos---jardim-botanico-jardim-botanico'
		#'https://www.ifood.com.br/delivery/rio-de-janeiro-rj/pizza-hut---barra-da-tijuca-barra-da-tijuca'
	]

	def start_requests(self):
		for url in self.start_urls:
			yield scrapy.Request(url, callback=self.parse, meta={
						          'splash': {
						                      'endpoint': 'render.html',
						                      'args': {'wait': 10.0}
						          }
						    })

	def parse_day_of_week(self, selector):
		dayOfWeek = DayOfWeek()
		dayOfWeek['week'] = selector.xpath('td[1]/text()').extract_first()
		hours = selector.xpath('td[2]/text()').extract_first().strip().split('\n')
		if hours[0] != '-':
			dayOfWeek['openAt'] = hours[0].strip()[:5].strip()
			dayOfWeek['closeAt'] = hours[1].strip()[:5].strip()
		else:
			dayOfWeek['openAt'] = None
			dayOfWeek['closeAt'] = None

		return dayOfWeek

	def parse_opening_hours(self, response):
		openingHours = []
		days_selector = response.xpath('//div[@class="tooltip-wrap hide-on-phone"]/div[@class="tooltip"]/table/tr')
		for day_selector in days_selector:
			openingHours.append(self.parse_day_of_week(day_selector))
		return openingHours

	def parse_geo(self, jsonld):
		geo = Geo()
		geo_root = jsonld.get('geo', {'latitude': None, 'longitude': None})

		try:
			geo['latitude'] = float(geo_root.get('latitude', None))
			geo['longitude'] = float(geo_root.get('longitude', None))
		except (TypeError, ValueError):
			geo = None

		return geo

	def parse_address(self, jsonld):
		address = Address()
		address_root = jsonld.get('address', None)
		if address_root == None:
			return None

		address['addressCountry'] = address_root.get('addressCountry', None)
		address['addressLocality'] = address_root.get('addressLocality', None)
		address['addressRegion'] = address_root.get('addressRegion', None)
		address['streetAddress'] = address_root.get('streetAddress', None)
		address['postalCode'] = address_root.get('addressCountry', None)
		return address

	def parse_menu_item(self, selector, dynamicItem):
		menuItem = MenuItem()
		menuItem['name'] = selector.xpath('*//div[@class="text-wrap"]/h4/text()').extract_first()
		menuItem['description'] = selector.xpath('*//div[@class="text-wrap"]/p/text()').extract_first()

		if dynamicItem is not None:
			menuItem['dimension'] = dynamicItem['name']
		try:
			menuItem['price'] = float(selector.xpath('div/div/@data-unit-price').extract_first())
		except (TypeError, ValueError):
			if dynamicItem is not None:
				menuItem['price'] = dynamicItem['price']
			else:
				menuItem['price'] = None
		return menuItem

	def parse_food_category(self, selector, dynamicItems):
		foodCategory = FoodCategory()
		dynamicItemsCategory = None
		foodCategory['category'] = selector.xpath('div/a/h3/text()').extract_first()

		if foodCategory['category'] is not None:
			foodCategory['category'] = foodCategory['category'].strip()
			if dynamicItems is not None and foodCategory['category'].lower() in dynamicItems:
				dynamicItemsCategory = dynamicItems[foodCategory['category'].lower()]

		menu_items_selector = selector.xpath('*//div[@class="list"]/ul/li')
		menu_items_selector += selector.xpath('*//div[@class="list"]/li')

		foodCategory['items'] = []
		for index, list_selector in enumerate(menu_items_selector):
			forms = list_selector.xpath('form')
			dynamicItem = None

			if dynamicItemsCategory is not None and index in dynamicItemsCategory:
				dynamicItem = dynamicItemsCategory[index]

			if(len(forms)==0):
				foodCategory['items'].append(self.parse_menu_item(list_selector, dynamicItem))
			for item_selector in forms:
				foodCategory['items'].append(self.parse_menu_item(item_selector, dynamicItem))

		if len(foodCategory['items']) == 0:
			return None
		return foodCategory

	def reFormatDynamicItems(self, items, positions):
		dynamicItems = {}
		for i, itemString  in enumerate(items):
			item = json.loads(itemString.replace("'",'"'))
			category = item['brand'].lower()
			if category not in dynamicItems:
				dynamicItems[category] = {}
			dynamicItems[category][int(positions[i])] = {'name': item['variant'], 'price': item['price']}
		return dynamicItems

	def getDynamicItems(self, response):
		reMenuItens = re.compile(r'menuItens\.push\((.*?)\);', re.DOTALL)
		rePosition = re.compile(r'position \+= (\d+);', re.DOTALL)

		scriptMenuItens =response.xpath('//script[@type="application/ld+json"]/preceding-sibling::script[1]')[0]
		items = scriptMenuItens.re(reMenuItens)
		positions = scriptMenuItens.re(rePosition)

		dynamicItems = self.reFormatDynamicItems(items, positions)
		if len(dynamicItems) == 0:
			return None
		return dynamicItems

	def parse_menu(self, response):
		menu = []
		dynamicItems = self.getDynamicItems(response)

		menu_div = response.xpath('//div[@class="info"]')
		menu_div += response.xpath('//div[@class="info "]')
		for item_selector in menu_div:
			category = self.parse_food_category(item_selector, dynamicItems)
			if category is not None:
				menu.append(category)

		return menu

	def parse_restaurant(self, response):
		restaurant = Restaurant()
		jsonld_text = response.xpath('//script[@type="application/ld+json"]/text()').extract_first()
		jsonld = {}
		if jsonld_text is not None:
			jsonld_text = re.sub(r',[\n\s]*\]',']',jsonld_text)
			jsonld = json.loads(jsonld_text)

		restaurant['name'] = jsonld.get('name', None)
		restaurant['servesCuisine'] = jsonld.get('servesCuisine', None)
		restaurant['url'] = jsonld.get('url', None)
		restaurant['telephone'] = jsonld.get('telephone', None)

		restaurant['description'] = response.xpath('//p[@class="big"]/text()').extract_first()
		restaurant['starsRating'] = response.xpath('//div[@class="rest-score"]/text()').extract_first()
		if isinstance(restaurant['starsRating'], str):
			restaurant['starsRating'] = restaurant['starsRating'].strip()

		restaurant['geo'] = self.parse_geo(jsonld)
		restaurant['address'] = self.parse_address(jsonld)
		restaurant['openingHours'] = self.parse_opening_hours(response)
		restaurant['menu'] = self.parse_menu(response)

		return restaurant

	def parse(self, response):
		print("URL::::: " + response.url)
		restaurant = self.parse_restaurant(response)
		return restaurant
