# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy

class DayOfWeek(scrapy.Item):
	week = scrapy.Field()
	openAt = scrapy.Field()
	closeAt = scrapy.Field()

class Geo(scrapy.Item):
	latitude = scrapy.Field()
	longitude = scrapy.Field()

class Address(scrapy.Item):
	addressCountry = scrapy.Field()
	addressLocality = scrapy.Field()
	addressRegion = scrapy.Field()
	streetAddress = scrapy.Field()
	postalCode = scrapy.Field()

class MenuItem(scrapy.Item):
	name = scrapy.Field()
	description = scrapy.Field()
	dimension = scrapy.Field()
	price = scrapy.Field()

class FoodCategory(scrapy.Item):
	category = scrapy.Field()
	items = scrapy.Field()

class Restaurant(scrapy.Item):
	name = scrapy.Field()
	servesCuisine = scrapy.Field()
	url = scrapy.Field()
	telephone = scrapy.Field()

	description = scrapy.Field()
	starsRating = scrapy.Field()

	address = scrapy.Field()
	geo = scrapy.Field()
	openingHours = scrapy.Field()
	menu = scrapy.Field()
