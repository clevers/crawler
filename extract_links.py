import io
from lxml import etree

with io.open("restaurants.xml") as dt:
    tree = etree.parse(dt)
    dt.close()

links_tree = tree.xpath("//loc")


all_restaurants_links = [ l.text for l in links_tree ]

rj = "https://www.ifood.com.br/delivery/rio-de-janeiro-rj/"
rio_restaurants_links = [ l.text for l in links_tree if rj in l.text ]
